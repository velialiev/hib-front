import React from 'react'
import { Formik } from 'formik'
import * as Yup from 'yup'
import classes from './style.module.scss'
import FormInput from '../../../../components/FormInput'
import FormCheckbox from '../../../../components/FormCheckbox'
import Button from '../../../../components/Button'
import useAppDispatch from '../../../../hooks/useAppDispatch'
import { login } from '../../authSlice'

const validationSchema = Yup.object().shape({
  email: Yup.string().required('Email is required'),
  password: Yup.string().required('Password is required'),
})

const AuthorizationForm = () => {
  const dispatch = useAppDispatch()

  return (
    <Formik
      initialValues={{
        email: '',
        password: '',
        rememberMe: false,
      }}
      validationSchema={validationSchema}
      onSubmit={({ email, password, rememberMe }) => {
        dispatch(login({ email, password, rememberMe }))
      }}
    >
      {({ handleSubmit }) => (
        <form className={classes.wrap} onSubmit={handleSubmit}>
          <h1 className={classes.title}>Sign in</h1>

          <FormInput wrapperClassName={classes.formItem} name="email" placeholder="Email" />

          <FormInput
            wrapperClassName={classes.formItem}
            name="password"
            placeholder="Password"
            type="password"
          />

          <FormCheckbox wrapperClassName={classes.rememberMe} name="rememberMe">
            Remember me
          </FormCheckbox>

          <Button type="submit" className={classes.formItem}>
            Sign in
          </Button>
        </form>
      )}
    </Formik>
  )
}

export default AuthorizationForm
