import Http from '../../app/Http'

class AuthService {
  /**
   * todo Add rememberMe logic when it will be added on backend side
   */
  public static login(username: string, password: string, rememberMe: boolean) {
    return Http.post('/login', `username=${username}&password=${password}`)
  }

  public static registration(user: RegistrationUserDto) {
    return Http.post('/registration', {
      email: user.email,
      password: user.password,
      confirmPassword: user.confirmPassword,
    })
  }
}

export interface User {
  // todo: определить интерфейс
}

interface RegistrationUserDto {
  email: string
  password: string
  confirmPassword: string
}

export default AuthService
