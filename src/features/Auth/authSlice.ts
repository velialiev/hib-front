import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { RootState } from '../../app/rootReducer'
import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import AuthService, { User } from './AuthService'
import handleThunk, {
  SliceLoadingBase,
  SliceErrorBase,
} from '@velialiev/redux-toolkit-handle-thunk'

const initialState: State = {
  loading: {},
  error: {},
}

type State = {
  loading: SliceLoading
  error: SliceError
  user?: User
}

const getSliceState = (state: RootState) => state.auth

export const authSelectors = {
  getUser: (state: RootState) => getSliceState(state).user,
}

const thunks = {
  login: createAsyncThunk('auth/login', ({ email, password, rememberMe }: LoginPayload) => {
    return AuthService.login(email, password, rememberMe)
  }),
}

export const { login } = thunks

interface LoginPayload {
  email: string
  password: string
  rememberMe: boolean
}

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    handleThunk(builder, login)
  },
})

const persistConfig = {
  key: 'auth',
  storage,
  whitelist: ['user'],
}

type ThunkNamesUnion = keyof typeof thunks
type SliceLoading = SliceLoadingBase<ThunkNamesUnion>
type SliceError = SliceErrorBase<ThunkNamesUnion>

export const {} = authSlice.actions
export default persistReducer(persistConfig, authSlice.reducer)
