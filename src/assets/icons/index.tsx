import { ReactComponent as ParameterIcon } from './parameter.svg'
import { ReactComponent as HomeIcon } from './home.svg'
import { ReactComponent as CartIcon } from './cart.svg'
import { ReactComponent as ProfileIcon } from './profile.svg'

export {
  ParameterIcon, HomeIcon, CartIcon, ProfileIcon, 
}
