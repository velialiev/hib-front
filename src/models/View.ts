import React from 'react'

type View = string | React.ReactNode | React.ReactNodeArray

export default View
