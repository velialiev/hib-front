import React, { PropsWithChildren } from 'react'
import { useField } from 'formik'
import Select, { Props as SelectProps } from '../Select'
import FormControl from '../FormControl'

export interface Props<T> extends SelectProps<T> {
  name: string
}

const FormSelect = <T, >({ name, ...props }: PropsWithChildren<Props<T>>) => {
  const [field, , helper] = useField(name)
  return (
    <FormControl name={name} customField>
      <Select {...props} {...field} onChange={(value) => helper.setValue(value)} />
    </FormControl>
  )
}

export default FormSelect
