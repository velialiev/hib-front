import React from 'react'
import { Link } from 'react-router-dom'
import classes from './Header.module.scss'
import Button from '../Button'
import {
  ParameterIcon, HomeIcon, CartIcon, ProfileIcon, 
} from '../../assets/icons'

const Header = () => {
  return (
    <div className={classes.header}>
      <div className={classes.nav}>
        <div>
          <Link to="/settings" className={classes.link}>
            <Button icon={<ParameterIcon height="25" />} />
          </Link>
          <Link to="/" className={classes.link}>
            <Button icon={<HomeIcon height="25" />} />
          </Link>
        </div>
        <div>
          <Link to="/cart" className={classes.link}>
            <Button icon={<CartIcon height="25" />}>Cart</Button>
          </Link>
          <Link to="/login" className={classes.link}>
            <Button icon={<ProfileIcon height="25" />}>Login</Button>
          </Link>
        </div>
      </div>
      <div className={classes.container}>
        <h1 className={classes.title}>ΠΑΛΑΙΑ & ΣΠΑΝΙΑ ΒΙΒΛΙΑ</h1>
        <p className={classes.description}>Old and rare books</p>
      </div>
    </div>
  )
}

export default Header
