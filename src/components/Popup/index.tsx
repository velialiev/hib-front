import React, { FC } from 'react'
import cn from 'classnames'
import { Modal as ModalAntd } from 'antd'
import { ModalProps as ModalAntdProps } from 'antd/lib/modal'
import classes from './Popup.module.scss'
import buttonClasses from '../Button/Button.module.scss'

export interface Props extends ModalAntdProps {}

export const Popup: FC<Props> = ({
  children,
  okButtonProps,
  cancelButtonProps,
  title = '',
  ...props
}) => {
  return (
    <ModalAntd
      {...props}
      title={title}
      okButtonProps={{ type: 'default', className: cn(buttonClasses.button), ...okButtonProps }}
      cancelButtonProps={{
        className: cn(buttonClasses.button, classes.cancelButton),
        ...cancelButtonProps,
      }}
    >
      {children}
    </ModalAntd>
  )
}
