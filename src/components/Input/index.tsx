import React, { FC } from 'react'
import { Input as InputAntd } from 'antd'
import { InputProps as InputAntdProps } from 'antd/lib/input'

export interface Props extends InputAntdProps {}

const Input: FC<Props> = ({ ...props }) => {
  return <InputAntd {...props} />
}

export default Input
