import React, { FC } from 'react'
import { Checkbox as CheckboxAntd } from 'antd'
import { CheckboxProps } from 'antd/lib/checkbox'
import cn from 'classnames'
import classes from './style.module.scss'

export interface Props extends CheckboxProps {}

const Checkbox: FC<Props> = ({ className, children, ...props }) => {
  const wrapperClass = cn(classes.checkbox, className)

  return (
    <CheckboxAntd {...props} className={wrapperClass}>
      {children}
    </CheckboxAntd>
  )
}
export default Checkbox
