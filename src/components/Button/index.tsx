import React, { FC } from 'react'
import cn from 'classnames'
import classes from './Button.module.scss'

interface Props extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  icon?: React.ReactNode
}

const Button: FC<Props> = ({
  icon, className, type = 'button', children, ...props 
}) => {
  return (
    <button {...props} type={type} className={cn(classes.button, className)}>
      {icon && <span className={cn({ [classes.iconMargin]: children })}>{icon}</span>}
      {children && <span className={classes.description}>{children}</span>}
    </button>
  )
}
export default Button
