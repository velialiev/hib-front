import React, { FC, DetailedHTMLProps, HTMLAttributes } from 'react'
import cn from 'classnames'
import classes from './index.module.scss'

interface Props extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  title: string
  img: string
}

const Card: FC<Props> = ({
  img, className, children, ...props 
}) => (
  <div {...props} className={cn(classes.card, className)}>
    <div className={classes.head}>
      <img src={img} className={classes.img} alt="" />
    </div>
    <div className={classes.body}>{children}</div>
  </div>
)

export default Card
