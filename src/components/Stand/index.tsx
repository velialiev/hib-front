import React, { FC } from 'react'
import cn from 'classnames'
import classes from './Stand.module.scss'

export interface Props
  extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement> {}

const Stand: FC<Props> = ({ children, className, ...props }) => {
  return (
    <div className={cn(classes.container, className)} {...props}>
      {children}
    </div>
  )
}

export default Stand
