import React, { PropsWithChildren } from 'react'
import { Select as SelectAntd } from 'antd'
import { SelectProps } from 'antd/lib/select'
import View from '../../models/View'

type Id = string | number

export interface Props<T> extends Omit<SelectProps<Id>, 'options' | 'value'> {
  options: T[]
  value?: Id
  resolveOptionId: (option: T) => Id
  resolveOptionContent: (option: T) => View
}

const { Option } = SelectAntd

const Select = <T, >({
  value,
  options,
  resolveOptionId,
  resolveOptionContent,
  ...props
}: PropsWithChildren<Props<T>>) => {
  return (
    <SelectAntd {...props} value={value?.toString()}>
      {options.map((option) => {
        const optionId = resolveOptionId(option).toString()

        return (
          <Option key={optionId} value={optionId}>
            {resolveOptionContent(option)}
          </Option>
        )
      })}
    </SelectAntd>
  )
}

export default Select
